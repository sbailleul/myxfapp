﻿using System;
using System.Collections.Generic;
using MyNoteKeeper.Models;

namespace MyNoteKeeper.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Note Note { get; set; }
        public IList<string> CourseList { get; set; }

        public String NoteHeading
        {
            get => Note.Heading;
            set
            {
                Note.Heading = value;
                OnPropertyChanged();
            }
        }
        public String NoteText
        {
            get => Note.Text;
            set
            {
                Note.Text = value;
                OnPropertyChanged();
            }
        }
        public String NoteCourse
        {
            get => Note.Course;
            set
            {
                Note.Course = value;
                OnPropertyChanged();
            }
        }

        
        
        public ItemDetailViewModel(Note note = null)
        {
            Title = "Edit note";
            InitializeCourseList();
            Note = note ?? new Note();
        }

        async void InitializeCourseList()
        {
            CourseList = await PluralsightDataStore.GetCoursesAsync();
        }
    }
}
