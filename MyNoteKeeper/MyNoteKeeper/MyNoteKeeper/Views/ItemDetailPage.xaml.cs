﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using MyNoteKeeper.Models;
using MyNoteKeeper.Services;
using MyNoteKeeper.ViewModels;

namespace MyNoteKeeper.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        private ItemDetailViewModel ViewModel { get; set; }
        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
            BindingContext = ViewModel;
        } 
        
        public ItemDetailPage()
        {
            InitializeComponent();
            ViewModel = new ItemDetailViewModel();
            BindingContext = ViewModel;
        }


        public void Cancel_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        public void Save_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "SaveNote", ViewModel.Note);
            Navigation.PopModalAsync();
        }

    }
}